import java.util.Arrays;

// spēles statuss & konfigurācija
State board;
ArrayList<Move> history = new ArrayList();
int userColor;
boolean userTurn;
boolean gameComplete;
int aiColor() {
  return other(userColor);
}
int currColor() {
  return userTurn? userColor : aiColor();
}


void newGame(boolean userFirst) {
  board = new State();
  userTurn = userFirst;
  userColor = userFirst? 1 : 2;
  history.clear();
  gameComplete = false;
}


int bw, bx, by; // board width, x, y
int leftWidth = 200;
void settings() {
  int border = 10;
  
  int screenHeight = 800;
  size(leftWidth + screenHeight, screenHeight);
  
  bw = screenHeight - border*2;
  bx = leftWidth + border;
  by = border;
}

boolean valid(int x, int y) {
  return x>=0 && x<8 && y>=0 && y<8;
}
int other(int col) {
  return col==1? 2 : 1;
}

class Move {
  final State s;
  final int x, y;
  final boolean user;
  Move(State s, int x, int y, boolean user) {
    this.s = s;
    this.x = x;
    this.y = y;
    this.user = user;
  }
  boolean none() { return x==-1; }
}
String moveString(int x, int y) {
  return x==-1? "--" : Character.toString('A'+x) + (1+y);
}
void makeMove(int x, int y, State s) {
  history.add(new Move(s, x, y, userTurn));
  board = s;
  userTurn^= true;
}
void gameEnd() {
  gameComplete = true;
}

boolean button(String text, int y0) {
  int x0 = 10;
  int x1 = leftWidth - x0;
  int y1 = y0 + 30;
  fill(30);
  rect(x0, y0, x1, y1);
  
  textSize(19);
  fill(220);
  textAlign(CENTER, CENTER);
  text(text, (x0+x1)/2, (y0+y1)/2 - 3);
  return !prevMousePressed && mousePressed && mouseX>=x0 && mouseX<x1 && mouseY>=y0 && mouseY<y1;
}


boolean prevMousePressed;
void draw() {
  int mx = Math.floorDiv((mouseX-bx) * 8, bw);
  int my = Math.floorDiv((mouseY-by) * 8, bw);
  
  background(22);
  rectMode(CORNERS);
  
  State next = null;
  State hoveredMove = null;
  fill(220);
  if (board==null) {
    textAlign(LEFT, TOP);
    textSize(20);
    if (button("Sāk lietotājs", 30)) newGame(true); // t.i. lietotājs ir tumšie kauliņi
    if (button("Sāk dators", 80)) newGame(false); // t.i. lietotājs ir gaišie kauliņi
  } else {
    // move logic
    if (!gameComplete) {
      if (board.possibleMoves(currColor()).length == 0) {
        makeMove(-1, -1, board);
        if (board.possibleMoves(currColor()).length == 0) gameEnd();
        delay(200);
      } else if (userTurn) {
        if (valid(mx, my) && userTurn) next = board.tryMove(mx, my, userColor);
        if (!prevMousePressed && mousePressed && next!=null) {
          makeMove(mx, my, next);
        }
      } else {
        aiThink();
      }
    }
    
    // tekstuālais spēles satusa apraksts
    textAlign(LEFT, TOP);
    textSize(16);
    if (gameComplete) {
      int score = board.evalFor(userColor);
      text(score>0? "Tu uzvarēji!" : score==0? "Neizšķirts." : "Dators uzvarēja.", 10, 5);
      if (button("Jauna spēle", 25)) board = null;
    } else {
      text(userTurn? "Tavs gājiens" : "Datora gājiens", 10, 5);
      
      text("Tu: "+board.count(userColor)+", dators: "+board.count(aiColor()), 10, 25);
    }
    
    // gājienu vēstures tabula
    {
      int eh = 20; // vienas rindas augstums
      textSize(16);
      for (int i = 0; i < (history.size()+(gameComplete? 1 : 2)) / 2; i++) {
        int y = 60 + i*eh;
        
        fill(30);
        stroke(10);
        rect(16, y, 50, y+eh);
        rect(50, y, 100, y+eh);
        rect(100, y, 150, y+eh);
        
        fill(220);
        textAlign(LEFT, TOP);
        text(i+". ", 20, y);
        
        textAlign(CENTER, TOP);
        for (int j = 0; j < 2; j++) {
          if (i*2 + j >= history.size()) break;
          Move m = history.get(i*2 + j);
          int x0 = j*50 + 50;
          text(moveString(m.x, m.y), x0 + 25, y);
          if (mouseX>=x0 && mouseX<x0+50 && mouseY>=y && mouseY<y+eh) hoveredMove = m.s;
        }
      }
    }
  }
  
  
  // spēles laukuma zīmēšana
  fill(30);
  stroke(10);
  translate(bx, by);
  rect(0, 0, bw, bw);
  for (int y = 0; y < 8; y++) {
    for (int x = 0; x < 8; x++) {
      int cx = bw*x/8;
      int cy = bw*y/8;
      fill(30);
      rect(cx, cy, bw*(x+1)/8, bw*(y+1)/8);
      int c, n;
      if (hoveredMove!=null) { // vēstures izvēlētā lauka parādīšana
        c = hoveredMove.get(x,y);
        n = 0;
      } else if (board!=null) {
        c = board.get(x,y);
        n = next==null? 0 : next.get(x,y);
      } else c = n = 0;
      if (c!=0 || n!=0) {
        fill((c==0? n : c)==1? 5 : 200, c==0? 128 : 256);
        ellipse(cx+bw/16, cy+bw/16, bw/10, bw/10);
      }
    }
  }
  
  // burti & cipari lauka malās
  fill(10);
  textSize(20);
  for (int i = 0; i < 8; i++) {
    textAlign(LEFT, BASELINE);
    text(Character.toString('A'+i), bw*i/8 + 3, bw - 3);
    textAlign(RIGHT, TOP);
    text(i+1, bw - 3, bw*i/8);
  }
  
  prevMousePressed = mousePressed;
}
