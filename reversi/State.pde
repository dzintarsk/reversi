static final int W = 8; 
class State {
  private final int[] board; // 0: nekas; 1: tumšs; 2: gaišs
  
  State() {
    board = new int[W*W];
    set(3,3,2);
    set(3,4,1);
    set(4,3,1);
    set(4,4,2);
  }
  
  State(State base) {
    board = Arrays.copyOf(base.board, W*W);
  }
  
  void set(int x, int y, int col) {
    assert valid(x, y) && (col>=0 && col<=2);
    board[y*8 + x] = col;
  }
  int get(int x, int y) {
    assert valid(x, y);
    return board[y*8 + x];
  }
  
  int evalFor(int col) { // heiristiskā funkcija
    return count(col) - count(other(col));
  }
  int count(int col) {
    int sum = 0;
    for (int i = 0; i < W*W; i++) sum+= board[i]==col? 1 : 0;
    return sum;
  }
  
  
  State tryMove(int x, int y, int col) {
    if (get(x,y)!=0) return null;
    
    State n = new State(this);
    n.set(x, y, col);
    boolean ok = false;
    for (int dx=-1; dx<=1; dx++) {
      for (int dy=-1; dy<=1; dy++) {
        int cx = x+dx;
        int cy = y+dy;
        boolean any = false;
        while (valid(cx, cy)) {
          int v = get(cx,cy);
          if (v==0) break;
          if (v==col) {
            if (!any) break;
            ok = true;
            while (cx!=x || cy!=y) {
              n.set(cx, cy, col);
              cx-= dx;
              cy-= dy;
            }
            break;
          }
          any = true;
          cx+= dx;
          cy+= dy;
        }
      }
    }
    return ok? n : null;
  }
  
  State[] possibleMoves(int col) {
    ArrayList<State> next = new ArrayList();
    for (int y = 0; y < 8; y++) {
      for (int x = 0; x < 8; x++) {
        State c = tryMove(x, y, col);
        if (c!=null) next.add(c);
      }
    }
    return next.toArray(new State[0]);
  }
  
  Pos recoverMove(State parent) {
    for (int y = 0; y < 8; y++) {
      for (int x = 0; x < 8; x++) {
        if ((get(x,y)!=0) != (parent.get(x,y)!=0)) return new Pos(x, y);
      }
    }
    throw new IllegalStateException("No move to recover");
  }
  
  
  
  String toString() {
    StringBuilder b = new StringBuilder();
    for (int y = 0; y < 8; y++) {
      for (int x = 0; x < 8; x++) {
        int col = get(x,y);
        b.append(col==0? '.' : col==1? 'x' : 'o');
      }
      b.append('\n');
    }
    return b.toString();
  }
  @Override int hashCode() {
    return Arrays.hashCode(board);
  }
  @Override boolean equals(Object o) {
    return o instanceof State && Arrays.equals(board, ((State)o).board);
  }
}
class Pos {
  final int x, y;
  Pos(int x, int y) {
    this.x = x;
    this.y = y;
  }
}
