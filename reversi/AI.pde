// pārlūkošana uz priekšu pār n gājieniem / minimaksa algoritms
final int DEPTH = 5;
void aiThink() {
  int sms = millis();
  
  // sākotnējais laukums
  Tree t0 = new Tree(board, userColor, history.size()==0? 0 : history.get(history.size()-1).none()? 1 : 0);
  
  ArrayList<Tree> currTrees = new ArrayList(); // pašreiz pēdējais slānis
  currTrees.add(t0);
  
  for (int i = 0; i < DEPTH; i++) {
    HashMap<State, Tree> map = new HashMap(); // duplikātu laukumu noteikšanai
    ArrayList<Tree> nextTrees = new ArrayList(); // saraksts ar jaunajiem apakškokiem
    for (Tree t : currTrees) {
      if (t.type==2) continue;
      State[] moves = t.curr.possibleMoves(t.chColor);
      boolean chNoop = false;
      if (moves.length==0) { // pašreizējam spēlētājam nav neviena iespējama gājiena
        if (t.type==1) { // arī iepriekšējam spēlētājiem nebija gājiena: spēle beidzas
          t.type = 2;
          continue;
        } else { // citādi, spēli turpina otrs spēlētājs
          moves = new State[]{t.curr};
          chNoop = true;
        }
      }
      
      Tree[] children = new Tree[moves.length];
      for (int j = 0; j < moves.length; j++) {
        State s = moves[j];
        if (map.containsKey(s)) { // laukums jau bija sasniegts no cita laukuma: atkārtoti to izmantojam
          children[j] = map.get(s);
        } else {
          Tree t2 = new Tree(s, t.chColor, chNoop? 1 : 0);
          children[j] = t2;
          nextTrees.add(t2);
          map.put(s, t2);
        }
      }
      t.children = children;
    }
    currTrees = nextTrees;
  }
  
  // atrod labāko gājienu
  int bestScore = Integer.MIN_VALUE;
  State bestState = null;
  for (Tree c : t0.children) {
    int score = c.minmax();
    if (score > bestScore) {
      bestScore = score;
      bestState = c.curr;
    }
  }
  if (bestState==null) throw new IllegalStateException("aiThink() called when no valid moves possible");
  
  // veic gājienu
  Pos p = bestState.recoverMove(board);
  makeMove(p.x, p.y, bestState);
  userTurn = true;
  
  // pagaida vismaz 0.2 sekundes
  int runtime = millis()-sms;
  println(runtime);
  if (runtime<200) delay(200-runtime);
}


class Tree {
  final State curr;
  final int chColor;
  Tree[] children; // ja null, tad dziļuma ierobežojums sasniegts; ja nav, būs vismaz viens elements
  int type; // 0: regulārs gājiens; 1: šis gājiens bija bez izmaiņām; 2: šis un nākamais gājiens ir bez izmaiņām (t.i. spēle beidzās) 
  int minmaxCache = Integer.MIN_VALUE; // minmax() rezultāta saglabātā vērtība, lai to nav jāpārrēķina vairākas reizes
  
  // rekursīvi izveido spēles koku
  Tree(State curr, int lastColor, int type) {
    this.curr = curr;
    this.chColor = other(lastColor);
    this.type = type;
  }
  
  void initChildren(Tree[] chBoards) {
    children = chBoards;
  }
  
  // atrod, ar minmax palīdzību, novērtējumu šij spēles pozīcijai AI spēlētājam
  int minmax() {
    if (minmaxCache!=Integer.MIN_VALUE) return minmaxCache;
    
    int aiCol = aiColor();
    if (type==2) {
      int score = curr.evalFor(aiCol);
      return score<0? score-10000 : score+10000; // ja ir spēles beigas redzamas, tad tā svaru palielināt virs gājieniem, kas vēl nebeidzās (bet mēģināt atrast opciju ar lielāko punktu skaitu)
    }
    
    boolean chAI = chColor==aiCol;
    if (children==null) return curr.evalFor(aiCol); // pieņem heiristikas vērtību dziļuma limita sasniegšanā
    
    // atrod minimumu vai maksimumu starp iespējamajiem gājieniem
    int bestScore = children[0].minmax();
    for (int i = 1; i < children.length; i++) {
      int score = children[i].minmax();
      bestScore = chAI? Math.max(bestScore, score) : Math.min(bestScore, score);
    }
    minmaxCache = bestScore;
    return bestScore;
  }
}
